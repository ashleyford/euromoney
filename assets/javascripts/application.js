
///////////////////////////
// What the hell is this?//
///////////////////////////

// Here i've defined an app variable
// where I can call my initalisation
// function. This method could be used for more
// complex logic but it always ensures the DOM
// is loaded and is just a demonstration of checking
// the DOM is ready to be manipulated using plain JavaScript
// of course this could easily be done using jQuery as we have
// it in the repo :)


    var animated = false;
    var animating = false;
    
var app = {

  init() {
    // call the navigation function
    navigation();
    $('.main').bind('scroll', function() {
      scroll();
    });
  }

};

// this is our navigation function that sets up the
// sidebar nav
function navigation(){
  var nav = responsiveNav('left-nav', {customToggle: '.nav-toggle'});
};

function scroll(){

    // not ideal as this will fire all the time on scroll
    // given a bit more time i'd optimise this like the below
    var position = $('.main').scrollTop();

    $("#article-header").css({
      // reduce opacity on scroll 500 represents the
      // basic height from the top of the page
      'opacity': 1 - ((position) / 500)
    });

    // This could also be done using CSS transforms
    // which would optimise the UX give a bit more time
    // i'd certainly change the way this works

    if(!animating) {
        if (position > 10) {
                if(!animated){
                    animating = true;
                    $('#category').animate({
                            left: -500
                    }, {"duration":500,"complete":complete});
                    animated = true;
                }
        } else if(animated){
                animating = true;
                $('#category').animate({
                    left: 0
                }, {"duration":150,"complete":complete} );
                animated = false;
        }
    }

}

function complete(){
    animating = false;
    scroll();
}

function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
};


// Run the app!
ready(app.init());
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Getting to grips with the blockchain - Euromoney - The BIG Stories</title>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
  <!-- main style -->
  <link rel="stylesheet" href="dist/css/application.css">

  <!-- Favicon and Mobile Icons -->

  <link rel="apple-touch-icon" sizes="180x180" href="dist/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="dist/images/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="dist/images/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="dist/images/favicons/manifest.json">
  <link rel="mask-icon" href="dist/images/favicons/safari-pinned-tab.svg" color="#364e7d">
  <link rel="shortcut icon" href="dist/images/favicons/favicon.ico">
  <meta name="msapplication-config" content="dist/images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <meta name="description" content="Banks have suddenly cottoned on to the power of the blockchain technology beneath Bitcoin. Inside their own treasuries and innovation labs, and increasingly in collaboration, banks are testing uses for rebranded distributed ledgers to replace their costly, proprietary systems.">
  <meta name="keywords" content="Euromoney, Bitcoin, Blockchain, Markets">
  <link rel="author" href="https://plus.google.com/{{googlePlusId}}" />
  <link rel="canonical" href="{{pageUrl}}" />

  <!-- Facebook Meta Tags -->

  <meta property="og:url" content="{{pageUrl}}">
  <meta property="og:image" content="{{imageUrl}}">
  <meta property="og:description" content="Banks have suddenly cottoned on to the power of the blockchain technology beneath Bitcoin. Inside their own treasuries and innovation labs, and increasingly in collaboration, banks are testing uses for rebranded distributed ledgers to replace their costly, proprietary systems.">
  <meta property="og:title" content="Getting to grips with the blockchain">
  <meta property="og:site_name" content="Euromoney">
  <meta property="og:see_also" content="http://euromoney.com">

  <!-- Google+ -->

  <meta itemprop="name" content="Getting to grips with the blockchain">
  <meta itemprop="description" content="Banks have suddenly cottoned on to the power of the blockchain technology beneath Bitcoin. Inside their own treasuries and innovation labs, and increasingly in collaboration, banks are testing uses for rebranded distributed ledgers to replace their costly, proprietary systems.">
  <meta itemprop="image" content="{{imageUrl}}">

  <!-- Twitter Card -->

  <meta name="twitter:card" content="summary">
  <meta name="twitter:url" content="{{pageUrl}}">
  <meta name="twitter:title" content="Getting to grips with the blockchain">
  <meta name="twitter:description" content="Banks have suddenly cottoned on to the power of the blockchain technology beneath Bitcoin. Inside their own treasuries and innovation labs, and increasingly in collaboration, banks are testing uses for rebranded distributed ledgers to replace their costly, proprietary systems.">
  <meta name="twitter:image" content="{{imageUrl}}">

  <!-- Google Analytics -->

  <script>
      var _gaq = _gaq || [];  
      _gaq.push(['_setAccount', 'UA-4XXXXXXX-1']);  
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
  </script>

</head>
<body>

    <!-- Main Container _navigation.scss -->

    <div role="navigation" id="left-nav" class="nav-collapse fadeIn">
    <!-- Euromoney Logo -->
      <a href="http://www.euromoney.com/" alt="Euromoney Magazine - Banking industry news & analysis of international finance
" id="logo"></a> 
      <ul>
        <li><a href="#">Markets</a></li>
        <li><a href="#">News and Opinion</a></li>
        <li class="active"><a href="#">The BIG Stories</a></li>
        <li><a href="#">Research and Awards</a></li>
        <li><a href="#">About Us</a></li>
        <!-- special 50/50 buttons -->
        <li class="inline-nav-btns"><a href="#">Free Trial</a></li>
        <li class="inline-nav-btns"><a href="#">Subscribe</a></li>
      </ul>
    </div>

    <!-- Main Container _container.scss -->

    <div role="main" class="main">

      <header id="article-header" class="fadeIn">
        <a href="#nav" class="nav-toggle"></a>
        <div id="category">The Big Stories | <b>Features</b></div>        
        <h1>Getting to grips with the blockchain</h1>
      </header>

      <section class="main-wrap">
        
        <div class="byline">
            <address class="author"><a rel="author" class="" href="#author/peter-lee" alt="Articles by Peter Lee - Euromoney.com">Peter Lee</a></address> 
            | <time pubdate datetime="2015-11-28" title="November 28th, 2015"> November 2015</time>
        </div>

        <p class="intro">Banks have suddenly cottoned on to the power of the blockchain technology beneath Bitcoin. Inside their own treasuries and innovation labs, and increasingly in collaboration, banks are testing uses for rebranded distributed ledgers to replace their costly, proprietary systems. Enthusiasts see banks creating a new fabric for payments transfer and financial markets, an internet of money. Doubters sense it’s all hype. Big challenges remain, but markets from private equity and syndicated loans to corporate bonds and derivatives may go on private blockchains within months.</p>

        <p>It's mid 2015 and Euromoney has been talking to the head of markets at one of the largest global banks about the changing structure of fixed income trading. As the meeting ends and we head to the lifts Euromoney asks what news from the OTC interest rate derivatives desks, now transitioning onto swap-execution facilities. </p>

        <h2>"The derivatives markets?</h2>

        <p>Oh, they’re all going OBC," says the banker. Euromoney makes that quizzical face we all learn in journalist training school. They’re doing what? "They're going on blockchain," the banker grins; the lift doors close and he ascends. In the days after, Euromoney tries to follow up but, unusually for him, the banker goes strangely silent.</p>

        <p>A couple of months later and the blockchain is all you hear from the banks. It is as if the entire industry has decided that the whole of finance is going on blockchain and all that remains to be decided is just how and when.
        Blockchain is the technological infrastructure underpinning bitcoin, and its association with the cryptocurrency has delayed appreciation among mainstream banks of the transformative power, elegance of design and genius of the mechanism for value exchange it embodies.</p>

        <p>At its heart, the blockchain is a series of incentives and limits for the creation, validation and secure maintenance of an open, shared, transparent and immutable ledger of record for all transactions and shifts in ownership in an asset: in the original case, the asset being bitcoins.</p>

        <h2>Participants in the bitcoin marketplace can:</h2>

        <ul class="fancyLi">
          <li>Spend their bitcoins</li>
          <li>Transfer them to other parties in payment for goods or services</li>   
          <li>Knowing that other participants in the network will recognize</li>
          <li>Validate that exchange or payment</li>
          <li>Record it in a system-wide ledger that no one central source owns or controls.</li>
        </ul>

        <blockquote>Participants in the bitcoin marketplace can spend their bitcoins <span class="quote-r"></span></blockquote>

        <article class="promo-story">
          <a href="#promo-story" alt="15 tips to increase your adwords profit - Euromoney.com">
            <h3>Promoted Story</h3>  
            <img src="dist/images/promo-story.jpg"  alt="15 tips to increase your adwords profit - Euromoney.com"/> 
            <p>15 tips to increase your<br/>adwords profit</p>
          </a>
        </article>

        <p>The so-called miners that perform this validation must show proof of work – in electricity consumed and computing power used – and are themselves paid in new bitcoins. They confirm each other’s work by consensus before a new page – or block – of the ledger is established, so ensuring that an owner of bitcoins cannot spend the same coins twice and that the receiver will not subsequently find his ownership of those transferred bitcoins challenged or his payment reneged on.</p>

        <p>incentives and limits for the creation, validation and secure maintenance of an open, shared, transparent and immutable ledger of record for all transactions and shifts in ownership in an asset: in the original case, the asset being bitcoins.</p>

        <p>participants in the bitcoin marketplace can spend their bitcoins – transfer them to other parties in payment for goods or services – knowing that other participants in the network will recognize and validate that exchange or payment and record it in a system-wide ledger that no one central source owns or controls.</p>

        <div class="article-image-wrap">
          <div class="article-image">
            <img src="dist/images/article-image.jpg" class="img-responsive" alt="Getting to grips with blockchain - Euromoney.com"/>
            <p rel="author">Lloyd Maxwell</p>
          </div>
        </div>

        <p>Banks are used to sitting on vast databases of proprietary data protected at the perimeter by password access. Blockchains embed encryption into every transaction and interaction between users and the ledger. Users have their own encrypted keys.

        <p>Blockchain technology uses encryption and a balance of incentives and checks such that the system as a whole can be trusted to work without any of the individual participants within it necessarily trusting each other – or even knowing each other beyond the lines-of-code pseudonyms that act as identifiers of individuals on the network.</p>

        <p>The so-called miners that perform this validation must show proof of work – in electricity consumed and computing power used – and are themselves paid in new bitcoins. They confirm each other’s work by consensus before a new page – or block – of the ledger is established, so ensuring that an owner of bitcoins cannot spend the same coins twice and that the receiver.</p>

      </section>

      <!-- _footer.scss -->

      <footer class="related-stories">

        <section class="inner-wrap">
        <h2>Related Stories</h2>

        <div class="column-row">

           <div class="column column-one">
            <div class="inner">
              <a href="#related-1" alt="Converter Ipod Video Taking Portable Video Viewing To A Whole New Level">
                <img src="dist/images/related-1.jpg" class="img-responsive" alt="Converter Ipod Video Taking Portable Video Viewing To A Whole New Level - Euromoney.com"/>
                <p>Converter Ipod Video Taking Portable Video Viewing To A Whole New Level</p>
              </a>
            </div>
           </div>

           <div class="column column-two">
            <div class="inner">
              <a href="#related-2" alt="Buying Used Electronic Test Equipment">
                <img src="dist/images/related-2.jpg" class="img-responsive" alt="Buying Used Electronic Test Equipment - Euromoney.com"/>
                <p>Buying Used Electronic Test Equipment What’s The Difference Between Used Refurbished Remarketed And Rebuilt</p>
              </a>
            </div>
           </div>

           <div class="column column-three">
            <div class="inner">
              <a href="#related-3" alt="Search Engine Optimization And Advertising">
              <img src="dist/images/related-3.jpg" class="img-responsive" alt="Search Engine Optimization And Advertising -Euromoney.com"/>
              <p>Search Engine Optimization And Advertising</p>
              </a>
            </div>
           </div>

        </div>

        <div class="column-row"> <!-- second row -->

           <div class="column column-one">
            <div class="inner">
              <a href="#related-4" alt="Using Banner Stands To Increase Trade Show Traffic">
                <img src="dist/images/related-4.jpg" class="img-responsive" alt="Using Banner Stands To Increase Trade Show Traffic - Euromoney.com"/>
                <p>Using Banner Stands To Increase Trade Show Traffic</p>
              </a>
            </div>
           </div>

           <div class="column column-two">
            <div class="inner">
              <a href="#related-5" alt="Differentiate Yourself And Attract More Attention Sales And Profits">
                <img src="dist/images/related-5.jpg" class="img-responsive" alt="Differentiate Yourself And Attract More Attention Sales And Profits - Euromoney.com"/>
                <p>Differentiate Yourself And Attract More Attention Sales And Profits</p>
              </a>
            </div>
           </div>

           <div class="column column-three">
            <div class="inner">
              <a href="#related-6" alt="Headset No Longer Wired For Sound">
                <img src="dist/images/related-6.jpg" class="img-responsive" alt="Headset No Longer Wired For Sound - Euromoney.com"/>
                <p>Headset No Longer Wired For Sound</p>
              </a>
             </div>
           </div>
        </div>

        </section>
      </footer>

      <!-- Legal Line Footer -->

      <footer class="legal">
        <div class="inner-wrap">
        <p>The material on this site is for financial institutions, professional investors and their professional advisers. It is for information only. Please read our Terms & Conditions, Privacy Policy and Cookies Policy before using this site.</p>
        <p>All material subject to strictly enforced copyright laws. &copy; 2016 Euromoney Institutional Investor PLC.</p>
        </div>
      </footer>

  </div>
  <!-- The main app script -->
  <script src="dist/js/application.js"></script>
</body>
</html>

# Setup

### Ashley Ford - front end development test

## Who Am I?

I’m a proactive individual with good interpersonal skills and am able to fit comfortably into any professional setting. I’m extremely motivated and always strives to push the boundaries to achieve my goal.
A keen blogger, designer, and developer, with startup and agency experience. I have a healthy interest in the Internet and it’s vast number of ever emerging technologies. You can check out my blog at [papermashup.com](http://papermashup.com/).

* [LinkedIn.com](https://uk.linkedin.com/in/ashleyford17)
* [Twitter.com](https://twitter.com/ashleyford)

## Working on the repository

[GruntJS](http://gruntjs.com/) is used for the build process, which means node and npm are required. If you already have those on your machine, you can install Grunt and all dependencies required for the build using:

```sh
npm install -g grunt-cli
npm install
```

[Bower](https://bower.io/) is also used to include dependencies. You can install/setup Bower using:

```sh
npm install -g bower
bower install
```

## Building The Project

To build the project, enter the following in terminal:

```sh
grunt
```

Grunt can also be used to monitor files and re-build the project on each change. For this we use Grunt's watch task:

```sh
grunt watch
```